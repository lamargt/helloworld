const test = (n)=>{
 	console.log('Run Async')
	return new Promise((res, rej)=>{
		setTimeout(function(){
			if(n === 10) res('Promise end')
			rej('Promises Error')
		}, 3000);
	})
}

console.log('Run First')
test(110).then(res=> console.log(res)).catch(err=>console.log(err))
console.log('Run Second')

